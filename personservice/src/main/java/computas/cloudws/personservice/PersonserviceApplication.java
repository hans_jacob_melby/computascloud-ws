package computas.cloudws.personservice;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;

@SpringBootApplication
@EnableDiscoveryClient
public class PersonserviceApplication {
	@Bean
	CommandLineRunner runner(PersonRepository repository) {
		return args -> {

			repository.deleteAll();

			ArrayList<Person> persons = new ArrayList<>();
			persons.add(new Person("John","Doe"));
			persons.add(new Person("Jane","Doe"));
			persons.add(new Person("Frodo","Baggins"));
			persons.add(new Person("Doctor","No"));
			persons.add(new Person("Doctor","Who"));







			persons.forEach(x -> repository.save(x));

			repository.findAll().forEach(System.out::println);
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(PersonserviceApplication.class, args);
	}
}
