# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Dette repositoriet er ment som et samle repository for en Spring-cloud workhop i regi av Computas AS
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Tasks to solve ###

* Create a config server running on port 8888
* Create your own service running on port 8080
* Make your service use config server to fetch configuration. 
* try to change the config in git. What happens?
* try to add secured information in git. Encrypt it and make configServer decrypt it for you.
* Add monitoring to your service.
* explore the api`s created for you
* Integrate with nameService from your service.
* Make your service a proxy for nameService.
* Secure your service with spring-security (basic auth is more than enough). 
How can you secure both naming service and your service by only securing your service ?
* make a call to nameService, and manipulate the response
* Make your service recilient with Hystrix
* set up ActuatorAdmin tool to have easy access to all applications. (status, trace etc)
* Make your service send data to the actuator admin tool
* set up a hystrix dashboard
* 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact