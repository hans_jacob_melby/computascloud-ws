package computas.cloudws.myservice;

import computas.cloudws.myservice.api.Person;
import computas.cloudws.myservice.api.PersonClient;
import computas.cloudws.myservice.api.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by hjm on 16.01.2017.
 */
@RestController
@RefreshScope
public class HelloController {


    private RestTemplate restTemplate;

    @Autowired
    private PersonService service;


    @Value("${message:Greetings from Spring Boot!}")
    private String message;

    @RequestMapping("/")
    public String index() {
        return message;
    }

    @RequestMapping("/findFirst")
    public Person findOnePerson() {

        return this.restTemplate.getForObject("http://localhost:9998/people/1", Person.class);
    }
    @RequestMapping("/findFirstRestTemplate")
    public Person findOnePersonUsingRestTemplate() {

        return this.restTemplate.getForObject("http://personservice/people/1", Person.class);
    }

    @RequestMapping("/findFirstFeign")
    public Person findOnePersonUsingFeign() {

        return service.getFirst();
    }

    @Autowired
    public HelloController(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }


}
