package computas.cloudws.myservice.api;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("personservice")
public interface PersonClient {
    @RequestMapping(value = "/people/1", method = RequestMethod.GET)
    Person getFirst();
}