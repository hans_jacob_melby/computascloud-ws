package computas.cloudws.myservice.api;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * Created by hjm on 29.01.2017.
 */
@Component
public class PersonService {

    private PersonClient personClient;


    @HystrixCommand(fallbackMethod = "fallback")
    public Person getFirst() {
        return personClient.getFirst();
    }
    public Person fallback() {
        return new Person("No","User");
    }

    @Autowired
    public PersonService(PersonClient client){
        this.personClient=client;
    }

}
